import React from 'react'
import PassingDataContext from '../../context/dataPassing';
import ReactEcharts from "echarts-for-react";

const averageTemperatureWidget = (props) => (
    <PassingDataContext.Consumer>
        {(context) => {
            let newData = []
            context.averageTemperature.forEach(item => {
                let a = {
                    device_display_name: '',
                    reading: []
                }
                a.device_display_name = item.device_display_name
                context.averageTemperature.forEach(ele => (
                    item['device_display_name'] === ele['device_display_name'] ? a['reading'].push(ele.reading) : null
                ))
                newData.push(a)
            })
            newData.forEach(item => {
                let sum = 0
                let average = 0
                item.reading.forEach(ele => {
                    sum += ele
                })
                average = sum / item.reading.length
                item['average'] = average
                return item
            })
            let obj = {}
            for (let i = 0; i < newData.length; i++) {
                obj[newData[i].device_display_name] = newData[i]
            }
            let dataArray = []
            for (let i in obj) {
                dataArray.push(obj[i])
            }
            const getOption = () => ({
                color: ['#3398DB'],
                title: {
                    text: 'Average Tempertaure',
                    subtext: 'Per device dispaly name'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '6%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: dataArray.map(item => item.device_display_name),
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: 'average',
                        type: 'bar',
                        barWidth: '50%',
                        data: dataArray.map(item => item.average)
                    }
                ]
            })
            return (
                <React.Fragment>
                    <h2 style={{ textAlign: "center", marginBottom: "3rem" }}>Below is the chart for Temperature average per device_display_name fro first 10 record</h2>
                    <ReactEcharts option={getOption()} />
                </React.Fragment>
            )
        }}
    </PassingDataContext.Consumer>
)

export default averageTemperatureWidget
