import React from 'react'
import PassingDataContext from '../../context/dataPassing';
import ReactEcharts from "echarts-for-react";

const summaryTable = () => (
    <PassingDataContext.Consumer>
        {(context) => {
            let newData = []
            context.summaryTable.forEach(item => {
                let a = {
                    device_display_name: '',
                    device_type: []
                }
                a.device_display_name = item.device_display_name
                context.summaryTable.forEach(ele => (
                    item['device_display_name'] === ele['device_display_name'] ? a['device_type'].push(ele.device_type) : null
                ))
                newData.push(a)
            })
            newData.forEach((item) => {
                item['count'] = []
                let count = {}
                item.device_type.forEach((x) => {
                    count[x] = (count[x] || 0) + 1;
                });
                item['count'].push(count)
            })
            let obj = {}
            for (let i = 0; i < newData.length; i++) {
                obj[newData[i].device_display_name] = newData[i]
            }
            let dataArray = []
            for (let i in obj) {
                dataArray.push(obj[i])
            }
            console.log(dataArray)
            const getOption = (item) => {
                return {
                    title: {
                        text: item.device_display_name,
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: '{a} <br/>{b} : {c} ({d}%)'
                    },
                    series: [
                        {
                            name: item.device_display_name,
                            type: 'pie',
                            radius: '65%',
                            center: ['50%', '60%'],
                            data: Object.keys(item.count[0]).map(ele => {
                                return { name: ele, value: item.count[0][ele] }
                            }),
                            emphasis: {
                                itemStyle: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                }
            }
            return (
                <div>
                    <h2 style={{textAlign: 'center', marginBottom: '4rem'}}>Below are the pie chart for all the devices with device type</h2>
                    {dataArray.map(item => (
                        <React.Fragment key={item.device_display_name}>
                            <ReactEcharts option={getOption(item)} />
                            <p style={{ border: "2px solid grey" }}></p>
                        </React.Fragment>
                    ))}
                </div>
            )
        }}
    </PassingDataContext.Consumer>
)

export default summaryTable
