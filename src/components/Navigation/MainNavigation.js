import React from 'react';
import { NavLink } from 'react-router-dom';

import './MainNavigation.css';

const mainNavigation = (props) => {
    return (
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>Oil Dashboard</h1>
            </div>
            <nav className="main-navigation-items">
                <ul>
                    <React.Fragment>
                        <li><NavLink to="/averagetemp">AverageTemerature</NavLink></li>
                        <li><NavLink to="/summarytable">SummaryTable</NavLink></li>
                    </React.Fragment>
                </ul>
            </nav>
        </header>
    )
}

export default mainNavigation