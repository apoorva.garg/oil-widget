import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import './App.css';

import MainNavigation from './components/Navigation/MainNavigation'
import HourlyTemperatureWidget from './components/HourlyTemperatureWidget/HourlyTemperatureWidget';
import AverageTemeratureWidget from './components/AverageTemperatureWidget/AverageTemperatureWidget';
import LiveTemperatureWidget from './components/LiveTemperatureWidget/LiveTemperatureWidget';
import SumamaryTable from './components//SummaryTable/SummaryTable';
import SampleData from './sampleData.json'
import PassingDataContext from './context/dataPassing';


class App extends Component {

  state = {
    data: []
  }

  componentDidMount() {
    // let currentIndex = 10;
    // setInterval(() => {
    //   this.setState({
    //     data: SampleData['data'].slice(0, currentIndex)
    //   })
    //   currentIndex += 10
    // }, 1000)
  }

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <PassingDataContext.Provider value={
            {
              averageTemperature: SampleData['data'].slice(0, 500),
              hourlyTemperature: SampleData['data'].slice(0, 10),
              summaryTable: SampleData['data'].slice(0, 500),
              liveTemperature: this.state.data
              // averageTemperature: this.state.data,
              // hourlyTemperature: SampleData['data'].slice(0, 10),
              // summaryTable: this.state.data,
              // liveTemperature: this.state.data
            }
          } >
            <MainNavigation />
            <main className="main-content">
              <Switch>
                <Redirect from="/" to="/hourtemp" exact />
                <Route path="/averagetemp" component={AverageTemeratureWidget} />
                <Route path="/summarytable" component={SumamaryTable} />
              </Switch>
            </main>
          </PassingDataContext.Provider>
        </React.Fragment>
      </BrowserRouter >
    );
  }
}

export default App;
