import React from 'react'

export default React.createContext({
    averageTemperature: [],
    hourlyTemperature: [],
    liveTemperature: [],
    summaryTable: []
});